selinux
=========

This module configures the SELinux run-mode and ensures that is not disabled in any way. Implemented recommendations are based on CIS Red Hat Enterprise Linux Benchmarks

Requirements
------------

None

Role Variables
--------------

enable_selinux: true - [controls if the module will run at all]

selinux_mode: "permissive" - [sets selinux mode]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
